﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

    public Camera myCamera;
    public Camera myArCamera;
    private Transform myCameraPosition;


    public Canvas canvasLogin;
    public Canvas canvasMain;
    public Canvas canvasChoose;
    public Canvas canvasBlood;
    public Canvas canvasEdit;
    public Canvas canvasScan;
    // Use this for initialization
    void Awake()
    {

        canvasLogin.GetComponent<Canvas>();
        canvasMain.GetComponent<Canvas>();
        canvasChoose.GetComponent<Canvas>();
        canvasBlood.GetComponent<Canvas>();
        canvasEdit.GetComponent<Canvas>();
        canvasScan.GetComponent<Canvas>();



        myCamera.GetComponent<Camera>();
        myArCamera.GetComponent<Camera>();
    }


    void Start()
    {
        canvasLogin.enabled = true;
        canvasMain.enabled = false;
        canvasChoose.enabled = false;
        canvasBlood.enabled = false;
        canvasEdit.enabled = false;
        canvasScan.enabled = false;

        myCameraPosition = myCamera.gameObject.transform;
        Debug.Log(myCameraPosition.position);


        myArCamera.gameObject.SetActive(false);
        myCamera.gameObject.SetActive(true);
    }





    public void GoToMain()
    {
        myCamera.transform.position = new Vector3(0, -50, myCameraPosition.position.z);
        canvasLogin.enabled = false;
        canvasMain.enabled = true;

    }

    public void GoToChose()
    {
        myCamera.transform.Rotate(0, 90, 0);
        canvasMain.enabled = false;
        canvasChoose.enabled = true;
    }

    public void GoToEdit()
    {
        myCamera.transform.Rotate(0, -90, 0);
        canvasMain.enabled = false;
        canvasChoose.enabled = false;
        canvasEdit.enabled = true;
    }


    public void GoToHome()
    {
        myCamera.transform.Rotate(0, 0, 0);
        myCamera.transform.position = new Vector3(0, 0, myCameraPosition.position.z);
        canvasMain.enabled = false;
        canvasChoose.enabled = false;
        canvasLogin.enabled = true;

    }

    public void GoToHomeBlood()
    {
        myCamera.transform.Rotate(0, -180, 0);
        myCamera.transform.position = new Vector3(0, 0, myCameraPosition.position.z);
        canvasMain.enabled = false;
        canvasLogin.enabled = true;
        canvasChoose.enabled = false;
        canvasBlood.enabled = false;


    }
    public void GoToHomeChoose()
    {
        myCamera.transform.Rotate(0, -90, 0);
        myCamera.transform.position = new Vector3(0, 0, myCameraPosition.position.z);
        canvasMain.enabled = false;
        canvasLogin.enabled = true;
        canvasChoose.enabled = false;
        canvasBlood.enabled = false;


    }

    public void GoToHomeEdit()
    {
        myCamera.transform.Rotate(0, 90, 0);
        myCamera.transform.position = new Vector3(0, 0, myCameraPosition.position.z);
        canvasMain.enabled = false;
        canvasLogin.enabled = true;
        canvasChoose.enabled = false;
        canvasBlood.enabled = false;
        canvasEdit.enabled = false;


    }

    public void GoToHomeScan()
    {
        myArCamera.gameObject.SetActive(false);
        myCamera.gameObject.SetActive(true);
        myCamera.transform.Rotate(0, 0, 0);
        myCamera.transform.position = new Vector3(0, 0, myCameraPosition.position.z);
        canvasMain.enabled = false;
        canvasLogin.enabled = true;
        canvasChoose.enabled = false;
        canvasBlood.enabled = false;
        canvasEdit.enabled = false;
        canvasScan.enabled = false;


    }


    public void GoToBlood()
    {
        myCamera.transform.Rotate(0, 90, 0);
        canvasMain.enabled = false;
        canvasChoose.enabled = false;
        canvasBlood.enabled = true;

    }

    public void GoToScan()
    {
        myArCamera.gameObject.SetActive(true);
        myCamera.gameObject.SetActive(false);
        canvasMain.enabled = false;
        canvasLogin.enabled = false;
        canvasChoose.enabled = false;
        canvasBlood.enabled = false;
        canvasEdit.enabled = false;
        canvasScan.enabled = true;

    }
}

